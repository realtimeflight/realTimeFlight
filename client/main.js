import angular from 'angular';
import angularMeteor from 'angular-meteor';
import nav from '../imports/client/components/nav/nav.component';
import googleMap from '../imports/client/components/googleMap/googleMap.component';
import CollectionService from '../imports/client/services/collection/collection.service';
import GoogleMapService from '../imports/client/services/googleMap/googleMap.service';
import StatsService from '../imports/client/services/Stats/stats.service'; //TODO : REMOVE
import stat from '../imports/client/components/stats/stats.component';

angular.module('myApp', [
	angularMeteor,
	// Components
	nav.name,
	googleMap.name,
	stat.name,
	// Providers
	CollectionService.name,
	GoogleMapService.name,
	//StatsService.name
]);// Map configuration
/*.config(function (uiGmapGoogleMapApi) {
  uiGmapGoogleMapApi.configure({
    key: "AIzaSyBp3vI8nhY9T7LhYyxwELFo1LO-iTdveQ4", //Clé pour utiliser l'API
    v: '3.17', //Par défaut la version la plus récente disponible
    libraries: 'weather,geometry,visualization', //Librairies supplémentaires
    language: 'en'
  });
});*/
