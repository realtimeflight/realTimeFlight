import { Meteor } from 'meteor/meteor';

import Collection from '../imports/api/collection/airplanesCollection';
import Mock from '../imports/api/collection/flightdata';
import { ApiTalkerFactory } from '../imports/api/api-talker/ApiTalkerFactory';
import Fiber from 'fibers';
/*import { MockApiTalker } from '../imports/api/api-talker/MockApiTalker';
import { ApiTalker } from '../imports/api/api-talker/ApiTalker';*/

Meteor.startup(() => {
	// code to run on server at startup
	console.log("Starting server...");
	removeFlightsPeriodically();

	// Add airports in the Airports database, if db is empty
	Collection.addAirports(function () {
		getApiData();
		setInterval(Meteor.bindEnvironment(function () {
			getApiData();
		}), 1000 * 30);
	});
});

//API Talker
var apitalker = ApiTalkerFactory().getApiTalker();


//Get Infos from apis :
function getApiData() {

	console.log("recup api data");
	var apitalker = ApiTalkerFactory().getApiTalker();
	apitalker.getFlights(function (data, err) { // ADS-B Exchange
		if (err) {
			console.error(JSON.stringify(err));
			return;
		}

		Fiber(function () {
			Collection.AddFlights(data, function (err, dataInDB) {//Ajoute a la DB
				//find and display Data in DB
				if (err) throw err;

			});
		}).run();
	});
}

//Send out the correct search info to the client side
Meteor.publish('search', function () {
	console.log("Publish");
	var self = this;

	// Add new planes in flights
	Collection.db.find({}).forEach(function (plane) {
		self.added('client-flights', plane._id, plane);
	});
	this.ready();
});

Meteor.publish('getAirports', function () {
	var self = this;

	// Add airports in client_airports
	Collection.dbAirports.find({}).forEach(function (airport) {
		self.added('client_airports', airport._id, airport);
	});
	this.ready();
});


/**
 * Removes all flights in the db once every 24h
 */
const removeFlightsPeriodically = function () {

	//1000 ms * 3600 s (= 1h) * 24 = 86400000
	setInterval(Meteor.bindEnvironment(function () {Collection.removeFlights()}), 86400000);
}


/*
*rend la méthode callable par le client
*/

Meteor.methods({

	getStats: function({from,dateBefore,dateNow}){

		var result = null;
		Collection.GetStatsAirport(from,dateBefore,dateNow,function(err, data){
			result = data;
		});
		return result;
	}
});
