/**
 * Returns the date with the time set to 0
 * It enables easier comparison
 * @param {*} date 
 */
export const resetTime = function (date){
    var localDate = new Date(date);
    localDate.setHours(0,0,0,0);
    return localDate;
}
