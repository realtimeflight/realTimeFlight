import { Meteor } from 'meteor/meteor';
import { assert, expect } from 'meteor/practicalmeteor:chai';
import { ApiTalkerFactory }from './ApiTalkerFactory.js';
import { MockApiTalker } from './MockApiTalker.js';
import { ApiTalker } from './ApiTalker.js';

var deep_equal = require('deep-equal');

if (Meteor.isServer) {
 describe('ApiTalker',  function () {
    describe('ApiTalkerFactory',  function () {
        it('makes sure ApiTalkerFactory return the right object',  function () {
            const factory = ApiTalkerFactory();
            factory.useMock = true;
            assert(deep_equal(factory.getApiTalker().toString(), MockApiTalker().toString()), "\n"+factory.getApiTalker().toString()+"\n!=\n"+MockApiTalker().toString()+"\n");
            factory.useMock = false;
            assert(deep_equal(factory.getApiTalker().toString(), ApiTalker().toString()));
        });
    });
    describe('MockApiTalker',  function () {
        it('makes sure MockApiTalker.getFlights works',  function () {
            var res = [
                {
                  "icao24":"89906f",
                  "callsign":"FEA301  ",
                  "squawk":"2622",//ID pour la requete dans flightdatabase
                  "origin_country":"Taiwan",
                  "from":"",//info de filght database
                  "to":"",//info de filght database
                  "position":{
                      "time_position":1509100049,
                      "last_contact":1509100050,
                      "longitude":119.2421,
                      "latitude":23.3617,
                      "geo_altitude":8526.78,
                      "on_ground":false,
                      "velocity":238.08,
                      "heading":237.89,//en degré decimal dans le sens des aiguille d'une montre depuis le NORD
                      "vertical_rate":0
                  }
                },
                {
                  "icao24":"7c6b2e",
                  "callsign":"",
                  "squawk":null,//ID pour la requete dans flightdatabase
                  "origin_country":"Australia",
                  "from":"",//info de filght database
                  "to":"",//info de filght database
                  "position":{
                      "time_position":1509100043,
                      "last_contact":1509100046,
                      "longitude":152.0318,
                      "latitude":-27.1376,
                      "geo_altitude":2148.84,
                      "on_ground":false,
                      "velocity":86.34,
                      "heading":118.86,//en degré decimal dans le sens des aiguille d'une montre depuis le NORD
                      "vertical_rate":0
                  }
                }
              ];
            MockApiTalker().getFlights(function(data){
                assert(deep_equal(data, res));
            });
        });
        it('makes sure MockApiTalker.getFlightData works',  function () {
            var callsign = "FDX1713 "
            var mock_data = 
            {
                "callsign":callsign,//ID pour la requete dans flightdatabase
                "from":{"fs":"CLT","iata":"CLT","icao":"KCLT","faa":"CLT","name":"Charlotte Douglas International Airport","street1":"5501 Josh Birmingham Parkway","city":"Charlotte","cityCode":"CLT","stateCode":"NC","postalCode":"28204","countryCode":"US","countryName":"United States","regionName":"North America","timeZoneRegionName":"America/New_York","weatherZone":"NCZ071","localTime":"2017-11-10T03:52:15.155","utcOffsetHours":-5.0,"latitude":35.219167,"longitude":-80.935833,"elevationFeet":748,"classification":1,"active":true,"delayIndexUrl":"https://api.flightstats.com/flex/delayindex/rest/v1/json/airports/CLT?codeType=fs","weatherUrl":"https://api.flightstats.com/flex/weather/rest/v1/json/all/CLT?codeType=fs"},
                "to":{"fs":"LHR","iata":"LHR","icao":"EGLL","name":"London Heathrow Airport","city":"London","cityCode":"LON","stateCode":"EN","countryCode":"GB","countryName":"United Kingdom","regionName":"Europe","timeZoneRegionName":"Europe/London","localTime":"2017-11-10T08:52:15.155","utcOffsetHours":0.0,"latitude":51.469603,"longitude":-0.453566,"elevationFeet":80,"classification":1,"active":true,"delayIndexUrl":"https://api.flightstats.com/flex/delayindex/rest/v1/json/airports/LHR?codeType=fs","weatherUrl":"https://api.flightstats.com/flex/weather/rest/v1/json/all/LHR?codeType=fs"},//info de filght database        
            };
            MockApiTalker().getFlightData(callsign,function(data){
                assert(deep_equal(data, mock_data));
            });
        });
        it('makes sure MockApiTalker.getFlightPosition works',  function () {
            var position = {
                "time_position":1509100049,
                "last_contact":1509100050,
                "longitude":119.2421,
                "latitude":23.3617,
                "geo_altitude":8526.78,
                "on_ground":false,
                "velocity":238.08,
                "heading":237.89,//en degré decimal dans le sens des aiguille d'une montre depuis le NORD
                "vertical_rate":0
            };
            MockApiTalker().getFlightPosition("89906f",function(data){
                assert(deep_equal(data, position));
            });
        });
        it('makes sure MockApiTalker.getAirportPosition works',  function () {
            var position = {
                "results" : [
                   {
                      "address_components" : [
                         {
                            "long_name" : "Brussels South Charleroi Airport",
                            "short_name" : "Brussels South Charleroi Airport",
                            "types" : [ "airport", "establishment", "point_of_interest" ]
                         },
                         {
                            "long_name" : "8",
                            "short_name" : "8",
                            "types" : [ "street_number" ]
                         },
                         {
                            "long_name" : "Rue des Frères Wright",
                            "short_name" : "Rue des Frères Wright",
                            "types" : [ "route" ]
                         },
                         {
                            "long_name" : "Charleroi",
                            "short_name" : "Charleroi",
                            "types" : [ "locality", "political" ]
                         },
                         {
                            "long_name" : "Hainaut",
                            "short_name" : "HT",
                            "types" : [ "administrative_area_level_2", "political" ]
                         },
                         {
                            "long_name" : "Wallonie",
                            "short_name" : "Wallonie",
                            "types" : [ "administrative_area_level_1", "political" ]
                         },
                         {
                            "long_name" : "Belgique",
                            "short_name" : "BE",
                            "types" : [ "country", "political" ]
                         },
                         {
                            "long_name" : "6041",
                            "short_name" : "6041",
                            "types" : [ "postal_code" ]
                         }
                      ],
                      "formatted_address" : "Brussels South Charleroi Airport (CRL), Rue des Frères Wright 8, 6041 Charleroi, Belgique",
                      "geometry" : {
                         "location" : {
                            "lat" : 50.4642198,
                            "lng" : 4.4649753
                         },
                         "location_type" : "ROOFTOP",
                         "viewport" : {
                            "northeast" : {
                               "lat" : 50.4655687802915,
                               "lng" : 4.466324280291502
                            },
                            "southwest" : {
                               "lat" : 50.4628708197085,
                               "lng" : 4.463626319708498
                            }
                         }
                      },
                      "place_id" : "ChIJ5dmW29AowkcR0mB242btosQ",
                      "types" : [ "airport", "establishment", "point_of_interest" ]
                   }
                ],
                "status" : "OK"
             };
            MockApiTalker().getAirportPosition("LIMC",function(data){
                assert(deep_equal(data, position));
            });
        });
            
    });
    describe('ApiTalker',  function () {
        var flights, position /*,airport, flightData*/;
        var icao24;
        var squawk = "6266";
        var callsign = "FDX1713"; 
        var flightData_error;

        before(function(allDone){
            this.timeout(15000);
            var getFlights = false, getFlightPosition = false, getAirportPosition = false, getFlightData = false;
            new ApiTalker().getFlights(function(q_flights){
                flights = q_flights;
                getFlights = true;
                done();

                var loadFlightPosition = function(i){
                    new ApiTalker().getFlightPosition(flights[i].icao24,function(position_,err){
                        if(err){
                            loadFlightPosition(i+1);
                            return;
                        }
                        icao24 = flights[i].icao24;
                        position = position_;
                        getFlightPosition = true;
                        done();
                    });
                }
                loadFlightPosition(0);
            });
            /*new ApiTalker().getAirportPosition("1600 Amphitheatre Parkway, Mountain View, CA",function(airport_){
                airport = airport_;
                getAirportPosition = true;
                done();
            });*/
            /*new ApiTalker().getFlightData(callsign,function(flightData_,error){
                flightData = flightData_;
                flightData_error = error;
                getFlightData = true;
                done();
            });*/
            function done(){
                if(getFlights && getFlightPosition /* && getAirportPosition && getFlightData*/){
                    allDone();
                }
            }
        });

        it('makes sure ApiTalker.getFlights return valid data structure',  function () {
            assert(flights != null,"Flights can't be null");
            var i = 0;
            flights.forEach(function(flight){
                assert(typeof flight.icao24 == "string" , "'icao24' must be a string");
                assert(TypeofOrNull(flight.callsign , "string") , "'callsign' must be a string or null");
                assert(TypeofOrNull(flight.squawk , "string") , "'squawk' must be a string or null");
                assert(typeof flight.origin_country == "string" , "'origin_country' must be a string");
                assert(typeof flight.position == "object" , "'position' must be a array");
                assert(TypeofOrNull(flight.position.time_position , "number") , "'position.time_position' must be a number or null");
                assert(typeof flight.position.last_contact == "number" , "'position.last_contact' must be a number");
                assert(TypeofOrNull(flight.position.longitude , "number") , "'position.longitude' must be a numbe or nullr");
                assert(TypeofOrNull(flight.position.latitude , "number") , "'position.latitude' must be a number or null");
                assert(TypeofOrNull(flight.position.geo_altitude , "number") , "'position.geo_altitude' must be a number or null");
                assert(TypeofOrNull(flight.position.on_ground , "boolean") , "'position.on_ground' must be a boolean");                
                assert(TypeofOrNull(flight.position.velocity , "number") , "'position.velocity' must be a number or null");
                assert(TypeofOrNull(flight.position.heading , "number") , "'position.heading' must be a number or null");
                assert(TypeofOrNull(flight.position.vertical_rate , "number") , "'position.vertical_rate' must be a number or null");
                i++;
            });
        });
        it('makes sure ApiTalker.getFlightPosition return the right data',  function () {
            assert(position != null,"position can't be null");
            for(var i = 0 ; i < flights.lenght ; i++){
                if(flights[i].icao24 == icao24){
                    assert(deep_equal(flights[i].position, position), "The received data seem incorrect");
                    return;
                }
            }
        });
        /*it('makes sure ApiTalker.getAirportPosition return valid data structure',  function () {
            assert(airport != null,"airport can't be null");
            var should_receive = {"results":[{"address_components":[{"long_name":"Google Building 42","short_name":"Google Bldg 42","types":["premise"]},{"long_name":"1600","short_name":"1600","types":["street_number"]},{"long_name":"Amphitheatre Parkway","short_name":"Amphitheatre Pkwy","types":["route"]},{"long_name":"Mountain View","short_name":"Mountain View","types":["locality","political"]},{"long_name":"Santa Clara County","short_name":"Santa Clara County","types":["administrative_area_level_2","political"]},{"long_name":"California","short_name":"CA","types":["administrative_area_level_1","political"]},{"long_name":"United States","short_name":"US","types":["country","political"]},{"long_name":"94043","short_name":"94043","types":["postal_code"]}],"formatted_address":"Google Bldg 42, 1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA","geometry":{"bounds":{"northeast":{"lat":37.42198310000001,"lng":-122.0853195},"southwest":{"lat":37.4214139,"lng":-122.0860042}},"location":{"lat":37.4216548,"lng":-122.0856374},"location_type":"ROOFTOP","viewport":{"northeast":{"lat":37.4230474802915,"lng":-122.0843128697085},"southwest":{"lat":37.4203495197085,"lng":-122.0870108302915}}},"place_id":"ChIJPzxqWQK6j4AR3OFRJ6LMaKo","types":["premise"]}],"status":"OK"};
            assert(deep_equal(airport,should_receive), "The received data seem incorrect");
        });*/
        /*it('makes sure ApiTalker.getFlightData return valid data structure',  function () {
            assert(flightData != null,"flightData can't be null : "+flightData_error);
            assert(flightData.callsign == callsign, "callsign received is different of callsign asked");
            assert(TypeofOrNull(flightData.from , "string") , "'from' must be a string or null");
            assert(TypeofOrNull(flightData.to , "string") , "'to' must be a string or null");
        });*/
    });
 });
}

function TypeofOrNull(v, type){
    return v == null || typeof v == type;
}