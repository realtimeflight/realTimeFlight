const mock_flights = [
  {
    "icao24":"89906f",
    "callsign":"FEA301  ",
    "squawk":"2622",//ID pour la requete dans flightdatabase
    "origin_country":"Taiwan",
    "from":"",//info de filght database
    "to":"",//info de filght database
    "position":{
        "time_position":1509100049,
        "last_contact":1509100050,
        "longitude":119.2421,
        "latitude":23.3617,
        "geo_altitude":8526.78,
        "on_ground":false,
        "velocity":238.08,
        "heading":237.89,//en degré decimal dans le sens des aiguille d'une montre depuis le NORD
        "vertical_rate":0
    }
  },
  {
    "icao24":"7c6b2e",
    "callsign":"",
    "squawk":null,//ID pour la requete dans flightdatabase
    "origin_country":"Australia",
    "from":"",//info de filght database
    "to":"",//info de filght database
    "position":{
        "time_position":1509100043,
        "last_contact":1509100046,
        "longitude":152.0318,
        "latitude":-27.1376,
        "geo_altitude":2148.84,
        "on_ground":false,
        "velocity":86.34,
        "heading":118.86,//en degré decimal dans le sens des aiguille d'une montre depuis le NORD
        "vertical_rate":0
    }
	}
  ];
  
  export var MockApiTalker = function() {
	var self = {};
  
	self.getFlightData = function(callsign, callback){
	  callback(
		{
		  "callsign":callsign,//ID pour la requete dans flightdatabase
		  "from":{"fs":"CLT","iata":"CLT","icao":"KCLT","faa":"CLT","name":"Charlotte Douglas International Airport","street1":"5501 Josh Birmingham Parkway","city":"Charlotte","cityCode":"CLT","stateCode":"NC","postalCode":"28204","countryCode":"US","countryName":"United States","regionName":"North America","timeZoneRegionName":"America/New_York","weatherZone":"NCZ071","localTime":"2017-11-10T03:52:15.155","utcOffsetHours":-5.0,"latitude":35.219167,"longitude":-80.935833,"elevationFeet":748,"classification":1,"active":true,"delayIndexUrl":"https://api.flightstats.com/flex/delayindex/rest/v1/json/airports/CLT?codeType=fs","weatherUrl":"https://api.flightstats.com/flex/weather/rest/v1/json/all/CLT?codeType=fs"},
		  "to":{"fs":"LHR","iata":"LHR","icao":"EGLL","name":"London Heathrow Airport","city":"London","cityCode":"LON","stateCode":"EN","countryCode":"GB","countryName":"United Kingdom","regionName":"Europe","timeZoneRegionName":"Europe/London","localTime":"2017-11-10T08:52:15.155","utcOffsetHours":0.0,"latitude":51.469603,"longitude":-0.453566,"elevationFeet":80,"classification":1,"active":true,"delayIndexUrl":"https://api.flightstats.com/flex/delayindex/rest/v1/json/airports/LHR?codeType=fs","weatherUrl":"https://api.flightstats.com/flex/weather/rest/v1/json/all/LHR?codeType=fs"},//info de filght database
		}
	  );
	}
  
	self.getFlightPosition = function(icao24, callback){
	  callback(mock_flights[0].position);
	}
  
	self.getFlights = function(callback){
	  callback(mock_flights);
	}
  
	self.getAirportPosition = function(airport,callback){
	  var mock_position ={
		"results" : [
		   {
			  "address_components" : [
				 {
					"long_name" : "Brussels South Charleroi Airport",
					"short_name" : "Brussels South Charleroi Airport",
					"types" : [ "airport", "establishment", "point_of_interest" ]
				 },
				 {
					"long_name" : "8",
					"short_name" : "8",
					"types" : [ "street_number" ]
				 },
				 {
					"long_name" : "Rue des Frères Wright",
					"short_name" : "Rue des Frères Wright",
					"types" : [ "route" ]
				 },
				 {
					"long_name" : "Charleroi",
					"short_name" : "Charleroi",
					"types" : [ "locality", "political" ]
				 },
				 {
					"long_name" : "Hainaut",
					"short_name" : "HT",
					"types" : [ "administrative_area_level_2", "political" ]
				 },
				 {
					"long_name" : "Wallonie",
					"short_name" : "Wallonie",
					"types" : [ "administrative_area_level_1", "political" ]
				 },
				 {
					"long_name" : "Belgique",
					"short_name" : "BE",
					"types" : [ "country", "political" ]
				 },
				 {
					"long_name" : "6041",
					"short_name" : "6041",
					"types" : [ "postal_code" ]
				 }
			  ],
			  "formatted_address" : "Brussels South Charleroi Airport (CRL), Rue des Frères Wright 8, 6041 Charleroi, Belgique",
			  "geometry" : {
				 "location" : {
					"lat" : 50.4642198,
					"lng" : 4.4649753
				 },
				 "location_type" : "ROOFTOP",
				 "viewport" : {
					"northeast" : {
					   "lat" : 50.4655687802915,
					   "lng" : 4.466324280291502
					},
					"southwest" : {
					   "lat" : 50.4628708197085,
					   "lng" : 4.463626319708498
					}
				 }
			  },
			  "place_id" : "ChIJ5dmW29AowkcR0mB242btosQ",
			  "types" : [ "airport", "establishment", "point_of_interest" ]
		   }
		],
		"status" : "OK"
	 };
	  callback( mock_position );
	}
  
	return self;
  }
  