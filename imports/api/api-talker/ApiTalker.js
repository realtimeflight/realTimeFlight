var request = require("request");

export function ApiTalker() {
	var self = {};

	self.getFlightPosition = function (icao24, callback) {
		get("https://opensky-network.org/api/states/all?icao24=" + icao24, function (body, error) {
			if (error) {
				callback(null, error);
				return;
			}

			body = JSON.parse(body).states;
			if(body == null){
				callback(null,"icao24 not found");
				return;
			}
			body = body[0];
			var position = {};

			position.time_position = body[3];
			position.last_contact = body[4];
			position.longitude = body[5];
			position.latitude = body[6];
			position.geo_altitude = body[7];
			position.on_ground = body[8];
			position.velocity = body[9];
			position.heading = body[10];
			position.vertical_rate = body[11];

			callback(position);
		});
	}
    
	self.getFlights = function (callback) {
		get("https://public-api.adsbexchange.com/VirtualRadar/AircraftList.json?fNoPosQ=true", function (body, error) {
			if (error) {
				callback(null, error);
				return;
			}
			var ret = [];
			body = JSON.parse(body).acList;
			body.forEach(function (raw) {

				if (raw.hasOwnProperty("From") && raw.hasOwnProperty("To")) {

					// Replace first ' ' by ',', then split by ','
					var airportFrom = raw.From.replace(' ', ',').split(',');
					var airportTo = raw.To.replace(' ', ',').split(',');

					var r = {};
					r.icao24 = raw.Icao;
					r.callsign = raw.Call;
					r.squawk = raw.Sqk;
					r.origin_country = raw.Cou;
					r.from = {
						icao: airportFrom[0],
						name: airportFrom[1]
					};
					r.to = {
						icao: airportTo[0],
						name: airportTo[1]
					};
					var position = {};

					position.time_position = raw.PosTime;
					position.last_contact = raw.TSecs;
					position.longitude = raw.Long;
					position.latitude = raw.Lat;
					position.geo_altitude = raw.GAlt;
					position.on_ground = raw.Gnd;
					position.velocity = raw.Spd;
					position.heading = raw.Trak;
					position.vertical_rate = raw.Vsi;

					r.position = position;
					// Info for googleMap InfoWindow
					r.aircraftModel = raw.Mdl;
					r.aircraftType = raw.Type;
					r.airlineCompany = raw.Op;
					r.airlineIcao = raw.OpIcao;

					ret.push(r);
				}
			});
			callback(ret);
		});
	}

	

	return self;
}

function get(url, callback, headers = {}) {
	request({
		uri: url,
		method: "GET",
		timeout: 10000,
		headers: headers,
		followRedirect: true,
		maxRedirects: 10
	}, function (error, response, body) {
		callback(body, error);
	});
}
