import { MockApiTalker } from './MockApiTalker.js';
import { ApiTalker } from './ApiTalker.js';

//const useMock = true;


export var ApiTalkerFactory = function(){
  var self = {};
  
  self.useMock = false;

  self.getApiTalker = function(){
    if(self.useMock){
      return MockApiTalker();
    }else{
      return ApiTalker();
    }
  }

  return self;
  
}
