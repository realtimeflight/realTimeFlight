import Ac from './airplanesCollection.js';

export const singleflightdata = [ {
    
         icao24 : "46bc37",
         callsign : "OAL011  ",
         squawk : "7027",
         origin_country : "Greece",
         from: {
            name: "Athens International Airport",
            icao : "LPPG"
        },
         to: {
            name: "Brussels South Charleroi Airport",
            icao : "EBBR"
        },
         position : {
            time_position : 1508487476,
            last_contact : 1508487479,
            longitude : 24.6232,
            latitude : 37.4022,
            geo_altitude : 2926.08,
            on_ground : false,
            velocity : 118.51,
            heading : 297.68,
            vertical_rate : 4.88
       }
    }]    

export const singleflightdata2 = {
    "states":[
        ["46bc37","OAL011  ","Greece",1000000000,1111111111,24.6232,37.4022,2926.08,false,118.51,297.68,4.88,null,3063.24,"7027",false,0]
    ]
}

export const multipleflightdata = [ {

     icao24 : "46bc37",
     callsign : "OAL011  ",
     squawk : "7027",
	 origin_country : "Greece",
	 from: {
        name: "Athens International Airport",
        icao : "LPPG"
    },
	 to: {
        name: "Brussels South Charleroi Airport",
        icao : "EBBR"
    },
	 toPosition: {},
     position : {
        time_position : 1508487476,
        last_contact : 1508487479,
        longitude : 20.6232,
        latitude : 30.4022,
        geo_altitude : 2926.08,
        on_ground : false,
        velocity : 118.51,
        heading : 297.68,
        vertical_rate : 4.88
   }
},
    {
    icao24 : "46bc37",
    callsign : "OAL011  ",
    squawk : "7027",
    origin_country : "Greece",
    from: {
        name: "Athens International Airport",
        icao : "LPPG"
    },
    to: {
        name: "Brussels South Charleroi Airport",
        icao : "EBBR"
    },
    toPosition: {},
    position : {
    time_position : 1508487476,
    last_contact : 1508487479,
    longitude : 24.6232,
    latitude : 37.4022,
    geo_altitude : 2926.08,
    on_ground : false,
    velocity : 118.51,
    heading : 297.68,
    vertical_rate : 4.88
    }
}
,
   {
    
         icao24 : "89906b",
         callsign : "EVA277  ",
         squawk : "6263",
		 origin_country : "Taiwan",
		 from: {
            name: "Brussels South Charleroi Airport",
            icao : "EBBR"
		},
		 to: {
            name: "Athens International Airport",
            icao : "LPPG"
		},
		 toPosition: {},
         position : {
            time_position : 1508487480,
            last_contact : 1508487480,
            longitude : 120.7389,
            latitude : 20.7614,
            geo_altitude : 10363.2,
            on_ground : false,
            velocity : 239.46,
            heading : 184.56,
            vertical_rate : 0
       }
    },
    {
        
             icao24 : "89906d",
             callsign : "EVA398  ",
             squawk : "1726",
             origin_country : "Taiwan",
             from: {
                name: "Brussels South Charleroi Airport",
                icao : "EBBR"
            },
             to: {
                name: "Athens International Airport",
                icao : "LPPG"
            },
             toPosition: {},
             position : {
                time_position : 1508487348,
                last_contact : 1508487354,
                longitude : 120.5198,
                latitude : 24.5441,
                geo_altitude : 4343.4,
                on_ground : false,
                velocity : 193.78,
                heading : 32.97,
                vertical_rate : -8.13
           }
        },
        {
            
                 icao24 : "3f5cd6",
                 callsign : "PACK22  ",
                 squawk : "1212",
                 origin_country : "Germany",
                 from: {
                    name: "Athens International Airport",
                    icao : "LPPG"
                },
                 to: {
                    name: "Brussels South Charleroi Airport",
                    icao : "EBBR"
                },
                 toPosition: {},
                 position : {
                    time_position : 1508487476,
                    last_contact : 1508487479,
                    longitude : 8.3421,
                    latitude : 49.3649,
                    geo_altitude : 10888.98,
                    on_ground : false,
                    velocity : 213.08,
                    heading : 125.07,
                    vertical_rate : -1.3
               }
            },

            {
                     icao24 : "7c6b2d",
                     callsign : "JST120  ",
                     squawk : "1162",
                     origin_country : "Australia",
                     from: {
                        name: "Athens International Airport",
                        icao : "LPPG"
                    },
                     to: {
                        name: "Brussels South Charleroi Airport",
                        icao : "EBBR"
                    },
                     toPosition: {},
                     position : {
                        time_position : 1508487479,
                        last_contact : 1508487479,
                        longitude : 155.0474,
                        latitude : -32.7704,
                        geo_altitude : 9730.74,
                        on_ground : false,
                        velocity : 221.08,
                        heading : 229.34,
                        vertical_rate : 0
                   }
                },

                {
                    icao24 : "aa9321",
                    callsign : "PACK22  ",
                    squawk : "0520",
                    origin_country : "United States",
                    from: {
                        name: "Brussels South Charleroi Airport",
                        icao : "EBBR"
                    },
                    to: {
                        name: "Athens International Airport",
                        icao : "LPPG"
                    },
                    toPosition: {},
                    position : {
                       time_position : 1508487480,
                       last_contact : 1508487480,
                       longitude : -74.0866,
                       latitude : 40.8189,
                       geo_altitude : 716.28,
                       on_ground : true,
                       velocity : 89.05,
                       heading : 205.68,
                       vertical_rate : -5.2
                  }
               }
]

export const geocodingAirportData = {
    "results" : [
       {
          "address_components" : [
             {
                "long_name" : "Brussels South Charleroi Airport",
                "short_name" : "Brussels South Charleroi Airport",
                "types" : [ "airport", "establishment", "point_of_interest" ]
             },
             {
                "long_name" : "8",
                "short_name" : "8",
                "types" : [ "street_number" ]
             },
             {
                "long_name" : "Rue des Frères Wright",
                "short_name" : "Rue des Frères Wright",
                "types" : [ "route" ]
             },
             {
                "long_name" : "Charleroi",
                "short_name" : "Charleroi",
                "types" : [ "locality", "political" ]
             },
             {
                "long_name" : "Hainaut",
                "short_name" : "HT",
                "types" : [ "administrative_area_level_2", "political" ]
             },
             {
                "long_name" : "Wallonie",
                "short_name" : "Wallonie",
                "types" : [ "administrative_area_level_1", "political" ]
             },
             {
                "long_name" : "Belgique",
                "short_name" : "BE",
                "types" : [ "country", "political" ]
             },
             {
                "long_name" : "6041",
                "short_name" : "6041",
                "types" : [ "postal_code" ]
             }
          ],
          "formatted_address" : "Brussels South Charleroi Airport (CRL), Rue des Frères Wright 8, 6041 Charleroi, Belgique",
          "geometry" : {
             "location" : {
                "lat" : 50.4642198,
                "lng" : 4.4649753
             },
             "location_type" : "ROOFTOP",
             "viewport" : {
                "northeast" : {
                   "lat" : 50.4655687802915,
                   "lng" : 4.466324280291502
                },
                "southwest" : {
                   "lat" : 50.4628708197085,
                   "lng" : 4.463626319708498
                }
             }
          },
          "place_id" : "ChIJ5dmW29AowkcR0mB242btosQ",
          "types" : [ "airport", "establishment", "point_of_interest" ]
       }
    ],
    "status" : "OK"
 }
 export const insertMockairport = function(callback){
    var airportTuple = {};
    // Example : 507,"London Heathrow Airport","London","United Kingdom","LHR","EGLL",51.4706,-0.461941,83,0,"E","Europe/London","airport","OurAirports"
			airportTuple._id = 507;
			airportTuple.name = "Brussels Airport zaventem";
			airportTuple.city = "Brussels";
			airportTuple.country = "Belgium";
			airportTuple.iata = "EBR";
			airportTuple.icao = "EBBR";
			airportTuple.latitude =4415.01454;
			airportTuple.longitude = -36.212;
			airportTuple.altitude = 2;
			airportTuple.timeZoneUTC = 1;
			airportTuple.DST = "E";
			airportTuple.timeZoneOlson = "Europe/London";
}

export const insertMockairports = function(callback){
    var airportTuple = {};
    // Example : 507,"London Heathrow Airport","London","United Kingdom","LHR","EGLL",51.4706,-0.461941,83,0,"E","Europe/London","airport","OurAirports"
			
			airportTuple.name = "Athens International Airport";
			airportTuple.city = "Athens";
			airportTuple.country = "Greece";
			airportTuple.iata = "LPG";
			airportTuple.icao = "LPPG";
			airportTuple.latitude =6615.01454;
			airportTuple.longitude = -6.32;
			airportTuple.altitude = 25;
			airportTuple.timeZoneUTC = 1;
			airportTuple.DST = "E";
            airportTuple.timeZoneOlson = "Europe/London";
    Ac.dbAirports.insert(airportTuple) ;

    airportTuple.name = "Brussels Airport zaventem";
    airportTuple.city = "Brussels";
    airportTuple.country = "Belgium";
    airportTuple.iata = "EBR";
    airportTuple.icao = "EBBR";
    airportTuple.latitude =4415.01454;
    airportTuple.longitude = -36.212;
    airportTuple.altitude = 2;
    airportTuple.timeZoneUTC = 1;
    airportTuple.DST = "E";
    airportTuple.timeZoneOlson = "Europe/London";

    Ac.dbAirports.insert(airportTuple) ;
    callback(null);

}