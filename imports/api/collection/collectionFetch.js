import { Meteor } from 'meteor/meteor';

const Flights = new Mongo.Collection('flights'); // Production DB
const Statistics = new Mongo.Collection('Statistics'); // Production DB
// Test DB
export const TestFlights = new Mongo.Collection('testFlights');
export const TestAirports = new Mongo.Collection('testAirports');
export const TestStatistics = new Mongo.Collection('testStatistics');

// PreProduction DB
const PreProdFlights = new Mongo.Collection('PreProdFlights');
const PreProdAirports = new Mongo.Collection('PreProdAirports');
const PreProdStatistics = new Mongo.Collection('PreprodStatistics');


export const getDbFlights = function(){
    if(Meteor.isTest){
        console.log("----> Test flights");
        return TestFlights;
    }
    console.log("----> prod flights");
    return Flights;
}

export const getDBStatistic = function(){
    if(Meteor.isTest){
        console.log("----> test stats");
        return TestStatistics;
    }
    console.log("----> Prod stats");
    return Statistics;
}

export const getDBAirports = function(){
    if(Meteor.isTest){
        console.log("----> Test airports");
        return TestAirports;
    }
    console.log("----> Prod airports");
    return PreProdAirports;
}