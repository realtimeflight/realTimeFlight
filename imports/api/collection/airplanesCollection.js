import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';
import ApiFactory from '../api-talker/ApiTalkerFactory.js';
import { readFileSync } from 'fs';
import Utils from '../utils.js';
import Collection from './collectionFetch.js';




//Switch between TestDb, Real Db and PreprodDB
export const db = Collection.getDbFlights();
export const dbAirports = Collection.getDBAirports();
export const dbStat = Collection.getDBStatistic();



/**
 * This works with a formatted json file passed as an argument to the function and
 * adds flights into the database or updates if it was already in the database.
 * The json file is formated by ApiTalker
 * @param {*} data
 * @param {*} callback
 */
export const AddFlights = function (data, callback) {
	var dataStored = [];
	for (i = 0; i < data.length; i++) {

		var flight = data[i];
		var isUpdated = false;
		//Updates an existing flight to avoid existing key errors
		if (db.findOne({ icao24: flight.icao24 }) != null) {
			//console.log('UPDATE !!!' + flight.icao24);
			isUpdated = true;
			db.update({ icao: flight.icao24 }, { $set: { from: flight.from, to: flight.to,position: flight.position } }, { upsert: true });
			dataStored.push(flight);
		}

		if (!isUpdated) {
			var errorFound = false;

			// From Airport
			GetAirportInfo(flight.from, (err, data) => {
				if (err) {
					errorFound = true;
					console.log(err.message);
				}
				flight.from = data;
			});

			// To Airport
			GetAirportInfo(flight.to, (err, data) => {
				if (err) {
					errorFound = true;
					console.log(err.message);
				}
				flight.to = data;
			});

			// if not error, we insert
			if (!errorFound) {
				_addStatData(flight.from,true, function(){

				});
				_addStatData(flight.to,false, function(){

				});
				db.insert(flight);
				dataStored.push(flight);
			}
		}
	}

	callback(null, dataStored);
}
/**
 * Removes Flights in the current Db
 */
export const removeFlights = function () {
	db.remove({});
	console.log("Remove All data from current DB now  - count  : " + db.find().count());
}

/**
 * Add Airports in the airports database
 * If db is empty => add, else => nothing
 */
export const addAirports = function (callback) {

	var airportsCount = dbAirports.find({}).count();
	// Adding airports
	if (airportsCount == 0) {
		console.log('Add airports in db');

		// The data is UTF-8 (Unicode) encoded.
		// ./assets/app/ is the directory after the build of meteor. /private => /assets/app of server
		var airportsCSV = readFileSync("./assets/app/airports.csv", "UTF-8");

		// Retrieve lines
		var airports = airportsCSV.split('\n');
		for (var i in airports) {
			// Retrieve data
			var airportData = airports[i].replace(new RegExp('"', 'g'), '').split(',');

			var airportTuple = {};
			//           0   1                         2        3                4     5      6       7         8  9 10  11              12        13
			// Example : 507,"London Heathrow Airport","London","United Kingdom","LHR","EGLL",51.4706,-0.461941,83,0,"E","Europe/London","airport","OurAirports"
			airportTuple._id = airportData[0];
			airportTuple.name = airportData[1];
			airportTuple.city = airportData[2];
			airportTuple.country = airportData[3];
			airportTuple.iata = airportData[4];
			airportTuple.icao = airportData[5];
			airportTuple.latitude = parseFloat(airportData[6]);
			airportTuple.longitude = parseFloat(airportData[7]);
			airportTuple.altitude = parseFloat(airportData[8]);
			airportTuple.timeZoneUTC = airportData[9];
			airportTuple.DST = airportData[10];
			airportTuple.timeZoneOlson = airportData[11];
			// Insert data
			dbAirports.insert(airportTuple);
		}
	}
	callback();
}

/**
 * Get position, city, country of the airport name given as an argument
 *
 * @param {*} airport Name of airport (by ADS-B Exhange)
 * @param {*} callback
 */

export const GetAirportInfo = function (airport, callback) {
	if (!airport) {
		callback(new Error("Airport From or To incorrect : " + airport), null);
		return;
	}

	var airportData = null;
	airportData = dbAirports.findOne({
		$or: [
			{ icao: airport.icao },
			{ name: { $regex: "^"+ airport.name +".*" } }
		]
	});
	if (airportData) {
		callback(null, airportData);
	} else {
		callback(new Error("Data airport not found : " + airport.icao), null);
	}
}

/**
 *
 * @param {*} data
 * @param {*} formOrTo if the plane is departing true else false
 * @param {*} callback
 */
const _addStatData = function(data, fromOrTo,callback){
	var statsUpdated =false;
	if(fromOrTo){
		var statsData = {
			date :new Date(),
			airport :{
				name : data.name,
				city : data.city,
				country : data.country,
				icao : data.icao,
				latitude : data.latitude,
				longitude : data.longitude
			}  ,
			arrivalCount : 0,
			departureCount : 1
			
		};
	}else{
		var statsData = {
			date :new Date(),
			airport :{
				name : data.name,
				city : data.city,
				country : data.country,
				icao : data.icao,
				latitude : data.latitude,
				longitude : data.longitude
			}  ,
			arrivalCount : 1,
			departureCount : 0

		};
	}
	
	var stat = dbStat.findOne({
		$or: [
			{ "airport.icao": statsData.airport.icao },
			{ "airport.name": { $regex: "^"+ statsData.airport.name +".*" } }
		]
	});

	if(stat){
		var currentDate  = new Date();
		var statDate = new Date(stat.date);
		if(statDate.getDate()== currentDate.getDate() && statDate.getMonth() == currentDate.getMonth() &&
				statDate.getFullYear() == currentDate.getFullYear()){
					statsUpdated = true;
			var dcount = stat.departureCount;
			var arrcount = stat.arrivalCount;
			if(fromOrTo){
				dbStat.update ({"airport.icao" : stat.airport.icao}, {$set : { departureCount : dcount +1 }});
			}
			else{
				dbStat.update ({"airport.icao" : stat.airport.icao}, {$set : {arrivalCount : arrcount +1 }});
			}
			
		}
		
	}
	if(!statsUpdated){
		dbStat.insert(statsData);
	}

	callback(null);
}

/**
 *
 * @param {*} name name of  city or country or airportname of the airport
 * @param {*} date1 from which the search should occur(can be null)
 * @param {*} date2 at which the search ends 
 * @param {*} callback 
 */
export const GetStatsAirport = function(name, date1, date2, callback){
	
	var stat = undefined;
	if(date1 == null){
		date1 = Utils.resetTime(date2);
	}else{
		date1 = Utils.resetTime(date1);
		//date2 = Utils.resetTime(date2);

	}
	console.log("-- "+ date1+"--"+ date2 );
	stat = dbStat.aggregate([{
		$match : {
			$or: [
				{ "airport.name": { $regex: "^"+ name +".*" }},
				{ "airport.country": { $regex: "^"+ name +".*" }},
				{ "airport.city": { $regex: "^"+ name +".*" }  }
			],
			date :  {"$gte": date1, "$lt" : date2}
		}},
		{$group : {_id: {  year: { $year: date1 }} ,
		departureCount :{ $sum : "$departureCount"},
		arrivalCount : {$sum : "$arrivalCount"},
		latitude : {$avg : "$airport.latitude"},
		longitude : {$avg : "$airport.longitude"},
		count : {$sum : 1}}
	}]);
	console.log("stats : " + stat);

	callback(null, stat);
}