import { Meteor } from 'meteor/meteor';
import { assert } from 'meteor/practicalmeteor:chai';
import Ac from './airplanesCollection.js';
import fd from './flightdata.js';
import Collection from './collectionFetch'

if (Meteor.isServer) {
    describe('FlightsDb', function () {
        before(function (done) {
            fd.insertMockairports(function (){
                done();
            });

        });
        after(function () {
            Collection.TestAirports.remove({});
        });

        /**
         * Test with One flight
         */
        describe('AddOneFlightMethod', function () {
            before(function (done) {
                Ac.AddFlights(fd.singleflightdata, function (err) {
                    if (err) throw err;
                    done();
                })


            });

            after(function () {
                Collection.TestFlights.remove({});
            });


            it('makes sure that there is at least one document in the database', function () {
                assert.equal(Collection.TestFlights.find({ callsign: "OAL011  " }).count(), 1);

            });

            it('makes sure the callsign gets into the database', function () {
                var fl = Collection.TestFlights.findOne({ callsign: 'OAL011  ' });
                assert.equal(fl.callsign, "OAL011  ");
            });

            it('makes sure origin_country gets into the database', function () {
                var fl = Collection.TestFlights.findOne({ origin_country: "Greece" })
                assert.equal(fl.origin_country, "Greece");

            });

            it('makes sure time-position gets into the database', function () {
                var fl = Collection.TestFlights.findOne({ "position.time_position": 1508487476 });
                assert.equal(fl.position.time_position, 1508487476);
            });


            it('makes sure velocity gets into the database', function () {
                var fl = Collection.TestFlights.findOne({ "position.velocity": 118.51 })
                assert.equal(fl.position.velocity, 118.51);

            });

            it('makes sure heading gets into the database', function () {
                var fl = Collection.TestFlights.findOne({ "position.heading": 297.68 })
                assert.equal(fl.position.heading, 297.68);

            });

            it('makes sure that the on_ground setting gets into the database', function () {
                var fl = Collection.TestFlights.findOne({ "position.on_ground": false })
                assert.equal(fl.position.on_ground, false);

            });

        });


        /**
         * Test with many flights
         */
        describe('AddManyFlightsMethod', function () {
            var flightsReturned = undefined;
            before(function (done) {
                Ac.AddFlights(fd.multipleflightdata, function (err, data) {
                    if (err) throw err;
                    flightsReturned = data;
                    done();
                });

            });

            after(function () {
                Collection.TestFlights.remove({});
            });

            it('makes sure there are 7 flights returned by the function ', function () {
                assert.equal(flightsReturned.length, 7);

            });

            it('makes sure that the first flight returned by the function has callsign OAL011  ', function () {
                assert.equal(flightsReturned[0].callsign, 'OAL011  ');

            });

            it('makes sure that the last flight returned by the function has callsign PACK22  ', function () {
                assert.equal(flightsReturned[6].callsign, 'PACK22  ');

            });


            it('makes sure that the flight EVA398 returned by the function has longitude : 120.5198 ', function () {
                assert.equal(flightsReturned[3].position.longitude, 120.5198);

            });

            it('makes sure that the flight EVA398 returned by the function has lattitude : 24.5441 ', function () {
                assert.equal(flightsReturned[3].position.latitude, 24.5441);

            });

            /*it('makes sure that there are seven documents in the database',  function () {
                   assert.equal(Ac.TestFlights.find().count(), 7);
            });*/

            it('makes sure the callsign OAL011 gets into the database', function () {
                var fl = Collection.TestFlights.findOne({ callsign: 'OAL011  ' });
                assert.equal(fl.callsign, "OAL011  ");

            });
            it('makes sure the callsign OAL011 is updated', function () {
                var fl = Collection.TestFlights.findOne({ callsign: 'OAL011  ' });
                assert.equal(fl.position.longitude, 20.6232);
                assert.equal(fl.position.latitude, 30.4022);

            });
            it('makes sure the callsign PACK22 gets into the database', function () {
                var fl = Collection.TestFlights.findOne({ callsign: 'PACK22  ' });
                assert.equal(fl.callsign, "PACK22  ");

            });
            it('makes sure the callsign EVA398 gets into the database', function () {
                var fl = Collection.TestFlights.findOne({ callsign: 'EVA398  ' });
                assert.equal(fl.callsign, "EVA398  ");
            });

            it('makes sure origin_country Greece gets into the database', function () {
                var fl = Collection.TestFlights.findOne({ origin_country: "Greece" })
                assert.equal(fl.origin_country, "Greece");

            });

            it('makes sure time-position 1508487476 gets into the database', function () {
                var fl = Collection.TestFlights.findOne({ "position.time_position": 1508487476 });
                assert.equal(fl.position.time_position, 1508487476);
            });


            it('makes sure velocity 118.51 gets into the database', function () {
                var fl = Collection.TestFlights.findOne({ "position.velocity": 118.51 })
                assert.equal(fl.position.velocity, 118.51);

            });

            it('makes sure heading 297.68 gets into the database', function () {
                var fl = Collection.TestFlights.findOne({ "position.heading": 297.68 });
                assert.equal(fl.position.heading, 297.68);

            });

            it('makes sure that the on_ground  setting false gets into the database', function () {
                var fl = Collection.TestFlights.findOne({ "position.on_ground": false });
                assert.equal(fl.position.on_ground, false);

            });

            it('makes sure that the on_ground setting true gets into the database', function () {
                var fl = Collection.TestFlights.findOne({ "position.on_ground": true });
                assert.equal(fl.position.on_ground, true);

            });


            it('makes sure that the flight JST120 has long : 155.0474', function () {
                var fl = Collection.TestFlights.findOne({ callsign: 'JST120  ' });
                assert.equal(fl.position.longitude, 155.0474);

            });

            it('makes sure that the flight JST120 has lattitude : -32.7704', function () {
                var fl = Collection.TestFlights.findOne({ callsign: 'JST120  ' });
                assert.equal(fl.position.latitude, -32.7704);

            });

        });

/*    describe('GetStats',  function () {
        var countData=undefined;
        before(function (done) {
                    Ac.AddFlights(fd.singleflightdata, function(err,data){
                        if(err) throw err;
                        Ac.GetStatsAirport("Brussels",null,new Date(), function(err, data){
                            countData = data;
                            done();
                        });

                    });

        });

        it('returns stats',  function () {
            return countData;
        });
        
    });*/

    describe('GetAirportInfoMethod',  function () {
          var infoReturned = undefined;
          before(function (done) {
              Ac.GetAirportInfo({icao : "EBBR", name : "Brussels Airport zaventem"}, function(err, data){
                  if(err) throw err;
                  infoReturned = data;
                  done();
              });

          });

          after(function () {
              Collection.TestAirports.remove({});
          });


          it('makes sure their is an airport inside the db ', function(){
              assert.equal( Ac.dbAirports.find({"name" :"Brussels Airport zaventem"}).count(), 1);
          });
  
          it('makes sure the airport name in db is Brussels Airport zaventem', function(){
              assert.equal(infoReturned.name, "Brussels Airport zaventem");
          });
  
          it('makes sure the airport city in db is Brussels', function(){
              assert.equal(infoReturned.city, "Brussels");
          });
  
          it('makes sure the airport country in db is Belgium', function(){
              assert.equal(infoReturned.country, "Belgium");
          });
  
          it('makes sure the airport longitude in db is -36.212', function(){
              assert.equal(infoReturned.longitude, -36.212);
          });
          
          it('makes sure the airport latitude in db is 4415.01454', function(){
              assert.equal(infoReturned.latitude,4415.01454 );
          });
      });
    
    });
}
