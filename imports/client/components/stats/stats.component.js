import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './stats.component.html';
import { Meteor } from 'meteor/meteor';
import { Tracker } from 'meteor/tracker';
import nav from '../nav/nav.component';

import './stats.component.css';

const search = {
	from: "Belgium",
	to: ""
}

function getMonday(d) {
    d = new Date(d);
    var day = d.getDay(),
        diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
    return new Date(d.setDate(diff));
  }




export default angular.module('statsLayout', [
	angularMeteor
]).controller('StatsCtrl', ['$scope', '$rootScope', function ($scope, $rootScope) {

    $scope.displayNone = true;
    $scope.displayFrom =false;
    $scope.displayTo = false;
    $scope.displayClear =false;
    $scope.from = "";
    $scope.to = "";
    $scope.statsShowed = $rootScope.statsShowed; 

   

    $scope.showStatsFromToday = function(){
        var date1 = new Date();
        
        Meteor.call('getStats', {
            from: $scope.from,
            dateBefore : null,
            dateNow : date1
           }, function(err,res) {
                if(err){
                    throw err;
                }else{
                    if(res){
                        var sumDeparture = res[0].departureCount;
                        var sumArrival = res[0].arrivalCount;
                        var lat = res[0].latitude;
                        var long = res[0].longitude;

                       $rootScope.addStatMarker("#0000FF", sumDeparture,sumArrival,lat,long);
                       $scope.displayClear =true;
                    }
                }
            }
        );
    }

    $scope.showStatsFromWeek = function(){
        var date1 = new Date();
        //find date of monday
        var date2 = getMonday(date1);

        Meteor.call('getStats', {
            from: $scope.from, 
            dateBefore : date2,
            dateNow : date1
           }, function(err,res) {
                if(err){
                    throw err;
                }else{
                    if(res){
                        var sumDeparture = res[0].departureCount;
                        var sumArrival = res[0].arrivalCount;
                        var lat = res[0].latitude;
                        var long = res[0].longitude;
                       $rootScope.addStatMarker("#009900", sumDeparture,sumArrival,lat,long);
                       $scope.displayClear =true;
                    }
                }
                
                
            }
        );
    }

    $scope.showStatsFromMonth = function(){
        var date1 = new Date();
        var month = date1.getMonth();
        var year = date1.getFullYear();
        var date2 = new Date(year,month,1);
        
        Meteor.call('getStats', {
            from: $scope.from,
            dateBefore : date2,
            dateNow : date1
           }, function(err,res) {
                if(err){
                    throw err;
                }else{
                    if(res){
                        var sumDeparture = res[0].departureCount;
                        var sumArrival = res[0].arrivalCount;
                        var lat = res[0].latitude;
                        var long = res[0].longitude;
                       $rootScope.addStatMarker("#FF0000", sumDeparture,sumArrival,lat,long);
                       $scope.displayClear =true;
                    }
                }
                
                
            }
        );
    }

    /*
    *TO :
    */
    $scope.showStatsToToday = function(){
        var date1 = new Date();
        
        Meteor.call('getStats', {
            from: $scope.to,
            dateBefore : null,
            dateNow : date1
           }, function(err,res) {
                if(err){
                    throw err;
                }else{
                    if(res){
                        var sumDeparture = res[0].departureCount;
                        var sumArrival = res[0].arrivalCount;
                        var lat = res[0].latitude;
                        var long = res[0].longitude;
                       $rootScope.addStatMarker("#0000FF", sumDeparture,sumArrival,lat,long);
                       $scope.displayClear =true;
                    }
                } 
            }
        );
    }

    $scope.showStatsToWeek = function(){
        var date1 = new Date();
        //find date of monday
        var date2 = getMonday(date1);

        Meteor.call('getStats', {
            from: $scope.to,
            dateBefore : date2,
            dateNow : date1
           }, function(err,res) {
                if(err){
                    throw err;
                }else{
                    if(res){
                        var sumDeparture = res[0].departureCount;
                        var sumArrival = res[0].arrivalCount;
                        var lat = res[0].latitude;
                        var long = res[0].longitude;
                       $rootScope.addStatMarker("#009900", sumDeparture,sumArrival,lat,long);
                       $scope.displayClear =true;
                    }
                }  
            }
        );
    }

    $scope.showStatsToMonth = function(){
        var date1 = new Date();
        var month = date1.getMonth();
        var year = date1.getFullYear();
        var date2 = new Date(year,month,1);
        
        Meteor.call('getStats', {
            from: $scope.to,
            dateBefore : date2,
            dateNow : date1
           }, function(err,res) {
                if(err){
                    throw err;
                }else{
                    if(res){
                        var sumDeparture = res[0].departureCount;
                        var sumArrival = res[0].arrivalCount;
                        var lat = res[0].latitude;
                        var long = res[0].longitude;
                       $rootScope.addStatMarker("#FF0000", sumDeparture,sumArrival,lat,long);
                       $scope.displayClear =true;
                    }
                }    
            }
        );
    }

    $scope.clearStats = function(){
        $rootScope.removeAllStatMarkers();
        $scope.displayClear =false;
    }

    $rootScope.displayStats = function(){
        $scope.statsShowed = !$scope.statsShowed;
    }

    $rootScope.toStats = function (from, to) {
        //reset
        $scope.displayNone = true;
        $scope.displayFrom =false;
        $scope.displayTo = false;
        $scope.from = "";
        $scope.to = "";


        search.from = from;
        search.to = to;

        $scope.from = search.from;
        $scope.to = search.to;
        
        if(search.from){
            $scope.displayNone = false;
            $scope.displayFrom =true;
        }
        if(search.to){
            $scope.displayNone = false;
            $scope.displayTo =true;
        }
    }

}])
	.component('statsLayout', {
        templateUrl: template,
       
    });

    
    