import angular from 'angular';
import angularMeteor from 'angular-meteor';
import 'angular-simple-logger';
import 'angular-google-maps';
import { Meteor } from 'meteor/meteor';

import template from './googleMap.component.html';
import { dbAirports } from '../nav/nav.component';

import './googleMap.component.css';

var deep_equal = require('deep-equal');


// App name
const name = 'googleMap';
const search = {
	from: "Belgium",
	to: ""
}

// planes
let dataPlanes = [];

// create a module
export default angular.module(name, [
	angularMeteor,
	'CollectionService',
	'GoogleMapService',
	'nemLogging',
	'uiGmapgoogle-maps'
]).controller("GoogleMapCtrl", ['$scope', '$rootScope', '$interval', '$http', 'uiGmapIsReady', 'Collection', 'AlgorithmPlane', 'GoogleMap',
function ($scope, $rootScope, $interval, $http, uiGmapIsReady, Collection, AlgorithmPlane, MapService) {

	// Map configuration
	$scope.map = {
		center: {
			latitude: 50.83,
			longitude: 4.35
		},
		zoom: 5,
		events: {},

		options: {
			// Position map type : ROADMAP / SATELLITE
			mapTypeControlOptions: {
				position: google.maps.ControlPosition.RIGHT_BOTTOM
			},
			fullscreenControl: false,
			streetViewControl: false
		}
	};

	/* === GOOGLE MAP IS READY ===
	-------------------------------------------------------------------------------*/
	uiGmapIsReady.promise(1).then(function (instances) {
		var map;
		instances.forEach(function (inst) {
			map = inst.map;
			//var uuid = map.uiGmap_id;
			//var mapInstanceNumber = inst.instance; // Starts at 1.
		});

		/*
		* Geolocation
		*/
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
				var pos = {
					lat: position.coords.latitude,
					lng: position.coords.longitude
				};
				var get = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='+ pos.lat +','+ pos.lng +'&key=AIzaSyBp3vI8nhY9T7LhYyxwELFo1LO-iTdveQ4&language=en';
				$http.get(get).then((response) => {
					var infoGeo = response.data;
					if (!infoGeo) {
						handleLocationError(true, map.getCenter());
						return;
					}

					infoGeo.results[0].address_components.forEach(function(a) {
						if (a.types[0] == "country") {
							search.from = a.long_name;
							$rootScope.notifyGeolocation(search.from);
						}
					});
					map.setCenter(pos);
				}, function() {
					console.log("Geocode not found");
				});
			}, function() {
				console.log("Geolocation not found");
			});
		} else {
			console.log("Navigator don't allow geolocation.")
		}

		initializationPlanes(false);
		// ineffective autorun, I prefer setInterval
		$interval(() => {
			initializationPlanes(false);
		}, 1000 * 10);

		/* Main Loop Movement */
		$interval(() => {
			for (var i in dataPlanes) {
				var plane = dataPlanes[i];
				// Update plane with velocity vector
				plane = AlgorithmPlane.createVelocityVector(plane);
				MapService.moveMarker(map, plane);
			}
		}, 1000);

		// Input : nav.component
		$rootScope.search = function (from, to) {
			search.from = from;
			search.to = to;
			initializationPlanes(true);
		}

		/* Initialization Planes on the map */
		function initializationPlanes(newResearch) {
			var planes = [];
			Collection.subscribe(search, (err, data) => {
				planes = data;
			});

			// if it's a new research => clear all markers
			if (newResearch) {
				MapService.clearAllMarkers(dataPlanes);
				dataPlanes = [];
			}
			if (planes.length != 0) {

				for (var i in planes) {
					var plane = planes[i];

					updatePlane(plane);
				}
				console.log(planes);
			}
		}

		/**
		* Find a plane in dataPlanes and update its infos
		* Else push the plane
		* @param {*} plane
		*/
		function updatePlane(plane) {

			for (var i in dataPlanes) {
				if (dataPlanes[i].icao24 == plane.icao24) {
					plane.marker = dataPlanes[i].marker;
					dataPlanes[i] = plane;
					return true;
				}
			}
			plane = MapService.createMarkerPlane(map, plane);
			dataPlanes.push(plane);
			return false;
		}

		// Stats marker loading ------------------------------------

		var markers = [];

		$rootScope.removeAllStatMarkers = function(){
			markers.forEach(function(elt){
				if(elt.marker != null){
					elt.marker.setMap(null);
				}
			});
			markers = [];
		}
		
		$rootScope.addStatMarker = function(color, departure, arrival, lat, long){
			var position = {latitude : lat, longitude : long};
			
			console.log("addStatMarker "+departure+" "+arrival);
			var newMarkerData = { color: color, position : position, departure : departure, arrival:arrival };
			markers.push(newMarkerData);
			refreshStatMarkers();
			console.log(markers);
		}

		function refreshStatMarkers(){
			var markers_tmp = [];
			console.log("refresh markers stat : ")
			console.log(markers);
			for(var i in markers){
				
				if(markers[i].marker != null){
					markers[i].marker.setMap(null);
				}

				markers_tmp.push(markers[i]);
			}
			markers = [];
			console.log("markers_tmp :");
			console.log(markers_tmp);

			for(var i in markers_tmp){
				MapService.createMarkerStat(map, markers_tmp[i], function(newMarkerData){
					markers.push(newMarkerData);
				});
			}
		}

		//$rootScope.addStatMarker();

		// Stats marker loading [END] -------------------------------

	});

}])
// Map Initialization
.component(name, {
	templateUrl: template,
	bindings: {
		location: '='
	},
});
