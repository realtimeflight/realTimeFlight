import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './nav.component.html';
import {
	Meteor
} from 'meteor/meteor';

import './nav.component.css';

import {
	Flights
} from '../googleMap/googleMap.component';

export const dbAirports = new Mongo.Collection('client_airports');

export default angular.module('navLayout', [
	angularMeteor
]).controller('NavCtrl', ['$scope', '$rootScope', function ($scope, $rootScope) {

	$scope.title = "RealTimeFlight";
	$scope.from = 'Belgium';
	$scope.to = '';
	$scope.planeslist = [];
	var citiesCountries = new Set();
	var airports = [];

	var interval = setInterval(() => {
		subscribe(true);
	}, 5000);

	function subscribe(newResearch) {
		var handler = Meteor.subscribe('getAirports');
		airports = dbAirports.find({}).fetch();
		for(var i in airports){
			citiesCountries.add(airports[i].city);
			citiesCountries.add(airports[i].country);
		}
		if(dbAirports.find({}).count()!=0){
			console.log("autocomplete completed");
			$scope.planeslist = Array.from(citiesCountries);
			handler.stop();
			clearInterval(interval);
		}
	}

	$rootScope.notifyGeolocation = function(from) {
		$scope.from = from;
	}

	//$scope.card = null;
	$scope.search = function (from, to) {
		// From : First Letters Uppercase
		from = firstLettersToUpper(from);
		// To : First Letters Uppercase
		to = firstLettersToUpper(to);
		// Modify Scope
		$scope.from = from;
		$scope.to = to;

		$rootScope.search(from, to);
		$rootScope.toStats(from, to);
	}

	//show stats
	$scope.showStats =function(){
		$rootScope.statsShowed = !$rootScope.statsShowed;
		$rootScope.displayStats();
	}

	function firstLettersToUpper(string) {
		if(string == "")
			return "";
		return string.toLowerCase()
		.split(' ')
		.map(function(word) {
			return word[0].toUpperCase() + word.substr(1);
		})
		.join(' ')
		.split('-')
		.map(function(word) {
			return word[0].toUpperCase() + word.substr(1);
		})
		.join('-');
	}

}])
.component('navLayout', {
	templateUrl: template,
});
