/**
 * Create google map element
 */
import 'angular-google-maps';

export class StatsService {
    enabled = true;

    /*
    * return a json with data to build a marker
    * [{ position : {latitude : 38.7755940, longitude : -9.1353670}, value : 2000 }]
    */
    getMarkersData(globalStatsData){
        return [
            { position : { latitude : 38.7755940, longitude : -9.1353670 }, value : 2000 },
            { position : { latitude : 45.7755940, longitude : 9.1353670 }, value : 3000 },
            { position : { latitude : 45.7755940, longitude : -9.1353670 }, value : 1000 },
            { position : { latitude : 38.7755940, longitude : 9.1353670 }, value : 200 }
        ];
    }
	
}
