import angular from 'angular';
import angularMeteor from 'angular-meteor';
import { Meteor } from 'meteor/meteor';

/*
* Client-side collections
* They aren't linked to servers, there is a difference between client-side and server-side collections.
*/
const Flights = new Mongo.Collection('client-flights'); // Custom DB for client

export default angular.module('CollectionService', [angularMeteor])
	.factory('Collection', function () {

		var collection = {};

		/**
		 * Server subscribe
		 * @param search Object
		 * @param callback function
		 */ 
		collection.subscribe = function (search, callback) {
			Meteor.subscribe('search');
			collection.findFlights(search, callback);
		}

		/**
		 * Find in the collection Flights
		 * (Separate of subscribe for the tests)
		 * @param search Object
		 * @param callback function
		 */ 
		collection.findFlights = function (search, callback) {
			var query = {};

			if (search.from != "" && search.to != "") {
				query = {
					$and: [
						{ $or: [{ "from.country": search.from }, { "from.city": search.from }] },
						{ $or: [{ "to.country": search.to }, { "to.city": search.to }] }
					]
				};
			}
			else if (search.from != "" && search.to == "") {
				query = {
					$or: [{ "from.country": search.from }, { "from.city": search.from }]
				};
			}
			else if (search.from == "" && search.to != "") {
				query = {
					$or: [{ "to.country": search.to }, { "to.city": search.to }]
				};
			}

			callback(null, Flights.find(query).fetch());
		}

		return collection;
	});