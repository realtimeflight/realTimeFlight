import 'angular';
import 'angular-mocks';

import { Meteor } from 'meteor/meteor';
import { assert, expect } from 'meteor/practicalmeteor:chai';
import './collection.service';

describe('CollectionService', function () {

	var c = null;

	beforeEach(window.module('CollectionService'));

	beforeEach(inject(function ($injector) {
		c = $injector.get('Collection');
	}))


	it('CollectionService should be initialized', function () {
		expect(c).to.not.be.null;
	});


	/**
	 * Test : Collection
	 */
	describe('Collection', function () {

		it('Collection ready and find 0 flight', function () {
			var search = {
				from: "",
				to: ""
			}
			c.findFlights(search, function(err, data) {
				assert.equal(data.length, 0);
			});
		});
	});
});
