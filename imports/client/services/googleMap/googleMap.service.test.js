import 'angular';
import 'angular-mocks';

import { Meteor } from 'meteor/meteor';
import { assert, expect } from 'meteor/practicalmeteor:chai';
import './googleMap.service';

describe('GoogleMapService', function () {

	var c = null;

	beforeEach(window.module('GoogleMapService'));

	beforeEach(inject(function ($injector) {
		c = $injector.get('GoogleMap');
		a = $injector.get('AlgorithmPlane');
	}))


	it('Service GoogleMap should be initialized', function () {
		expect(c).to.not.be.null;
	});

	it('Service AlgorithmPlane should be initialized', function () {
		expect(a).to.not.be.null;
	});

	/**
	 * Test : GoogleMap
	 */
	/*describe('GoogleMap', function () {

		it('Collection ready and find 0 flight', function () {
			var search = {
				from: "",
				to: ""
			}
			c.findFlights(search, function(err, data) {
				assert.equal(data.length, 0);
			});
		});
	});*/

	/**
	 * Test : AlgoPlane
	 */
	/*describe('AlgorithmPlane', function () {
		
				it('Collection ready and find 0 flight', function () {
					var search = {
						from: "",
						to: ""
					}
					c.findFlights(search, function(err, data) {
						assert.equal(data.length, 0);
					});
				});
			});*/
});
