/**
 * Create google map element
 */
import angular from 'angular';
import angularMeteor from 'angular-meteor';
import 'angular-google-maps';

const offsetRotation = 135;

export default angular.module('GoogleMapService', [angularMeteor])
	.factory('GoogleMap', function () {

		var googleMap = {};

		
		/**
		 * Create a marker in relation to stat on the map
		 * @param {*} stat 
		 */
		googleMap.createMarkerStat = function(map, stat, callback) {
			console.log(stat);
			if (stat == '' || !stat) {
				return;
			}

			var marker;

			//console.log("create marer stat (min & max) : "+min+" "+max);
			
			//var bounds = new google.maps.LatLngBounds();

			generateStatIcon(stat.color, stat.departure,stat.arrival, function(src) {
				console.log(src);
				var pos = new google.maps.LatLng(stat.position.latitude, stat.position.longitude);
				marker = new google.maps.Marker({
					position: pos,
					map: map,
					icon: src,
					zIndex : 1000
				});
				marker.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
				stat.marker = marker;
				callback(stat);
			});
		}

		var selectedAirplane = null;
		/**
		 * Create a marker in relation to plane data on the map
		 * @param {*} plane
		 * @param {*} map
		 */
		googleMap.createMarkerPlane = function (map, plane) {
			if (plane == '' || !plane) {
				return;
			}

			// !!! Rotation -135° because icon isn't to 0° but 135° by default
			/* DON'T TOUCH ICON PARAMS !
			 * The plane rotates correctly on its center
			 */
			var marker = new google.maps.Marker({
				position: {
					lat: plane.position.latitude,
					lng: plane.position.longitude
				},
				map: map,
				zIndex : 1,
				icon: {
					anchor: new google.maps.Point(800, 800),
					scale: 0.016,
					fillColor: 'black',
					fillOpacity: 1,
					strokeWeight: 0,
					strokeColor: 'black',
					path: 'M1090 1244 c-56 -28 -97 -61 -190 -153 l-118 -117 -69 20 c-56 17 -68 24 -65 39 5 24 -35 67 -62 67 -11 0 -33 -12 -48 -27 -28 -26 -28 -26 -28 -4 0 26 -42 71 -66 71 -10 0 -28 -10 -41 -22 -22 -20 -27 -21 -66 -10 -23 7 -63 17 -90 24 l-48 12 -39 -39 c-67 -65 -64 -70 134 -180 111 -62 189 -113 217 -141 l43 -44 -42 -47 c-23 -27 -75 -87 -114 -135 -83 -99 -73 -96 -248 -78 -86 9 -97 9 -120 -8 -17 -13 -25 -29 -25 -48 0 -27 8 -33 100 -79 99 -49 100 -51 105 -90 5 -38 7 -40 45 -45 39 -5 41 -6 90 -105 46 -92 52 -100 79 -100 19 0 35 8 48 25 17 23 17 34 8 120 -18 175 -21 165 78 248 48 39 108 91 135 114 l47 42 44 -43 c27 -26 79 -106 140 -216 111 -199 116 -202 181 -135 l39 39 -12 48 c-7 27 -17 67 -24 90 -11 39 -10 44 10 66 12 13 22 31 22 41 0 24 -45 66 -71 66 -22 0 -22 0 4 28 15 15 27 37 27 48 0 27 -43 67 -67 62 -15 -3 -22 9 -39 65 l-20 69 117 118 c92 93 125 134 153 190 38 76 48 154 24 178 -24 24 -102 14 -178 -24z',
					rotation: plane.position.heading - offsetRotation
				},
			});
			//polyline
			var flightPlanCoordinates = [{
					lat: plane.from.latitude,
					lng: plane.from.longitude
				},
				marker.getPosition(),
				{
					lat: plane.to.latitude,
					lng: plane.to.longitude
				}
			];;
			/*console.log("avion : " + marker.getPosition());
			console.log("origine lat : " + plane.from.latitude + "origine lng : " + plane.from.longitude);*/
			var flightPath = new google.maps.Polyline({
				path: flightPlanCoordinates,
				geodesic: true,
				strokeColor: '#F08080',
				strokeOpacity: 1.0,
				strokeWeight: 2
			});;

			// Marker listen to click event
			marker.addListener('click', function () {
				var content = '<table class="info-window">'
							+ ((plane.airlineCompany != null) ?
							'<tr>'
							+	'<th colspan="2" class="center"><img src="https://global.adsbexchange.com/VirtualRadar/images/File-'+plane.icao24+'%7C'+plane.airlineIcao+'/OpFlag.png" alt="'+plane.airlineCompany+'" title="'+plane.airlineCompany+'" width="90px"/></td>'
							+ '</tr>'
							+ '<tr>'
							+	'<th rowspan="2" class="b-right">Aircraft</th>'
							+	'<td>'+ plane.aircraftModel +'</td>'
							+ '</tr>'
							+ '<tr>'
							+	'<td class="center aircraft-type"><img src="https://global.adsbexchange.com/VirtualRadar/images/File-'+plane.icao24+'%7C'+plane.aircraftType+'/Type.png" alt="'+ plane.model +'"/></td>'
							+ '</tr>'
							: '')
							+ '<tr>'
							+	'<th class="b-right">From</th>'
							+	'<td>'+ plane.from.city + ', ' + plane.from.country +'</td>'
							+ '</tr>'
							+ '<tr>'
							+	'<th class="b-right">To</th>'
							+	'<td>' + plane.to.city + ', ' + plane.to.country + '</td>'
							+ '</tr>'
							+ '</table>';
				var window = new google.maps.InfoWindow({
					content: content,
					position: marker.getPosition(),
					maxWidth: '300'
				});
				if (selectedAirplane != null) {
					selectedAirplane.window.close();
					selectedAirplane.flightPath.setVisible(false);
				}
				selectedAirplane = marker;

				window.open(map);
				//set the plyline
				flightPath.setMap(map);
				flightPath.setVisible(true);
				//maj pos
				var windowRefresh = setInterval(function () {
					window.setPosition(marker.getPosition());
					//maj pos poly
					flightPath.setMap(null);
					flightPlanCoordinates[1] = plane.marker.getPosition();
					flightPath.setPath(flightPlanCoordinates);
					flightPath.setMap(map);
				}, 100);
				//permet de annuler l'interval quand on ferme la window
				window.addListener('closeclick', function () {
					clearInterval(windowRefresh);
					//unset the plyline
					flightPath.setMap(null);
					flightPath.setVisible(false);
				});
				marker.window = window;
				marker.windowsRefresh = windowRefresh;
			});
			marker.flightPath = flightPath;
			plane.marker = marker;
			return plane;
		}

		/**
		 * Move the marker on the map
		 * @param {*} map
		 * @param {*} plane
		 */
		googleMap.moveMarker = function (map, plane) {
			// if plane don't have a marker
			if (!plane.hasOwnProperty("marker")) {
				plane = this.createMarkerPlane(map, plane);
			}

			// Position API changed
			var timeMs = (new Date()).getTime() - plane.position.time_position;
			// Hardcoding Modification
			if (timeMs < 6000) {
				// Update position
				plane.marker.setPosition({
					lat: plane.position.latitude,
					lng: plane.position.longitude
				});

				// Update rotation
				var symbol = plane.marker.getIcon();
				symbol.rotation = plane.position.heading - offsetRotation;
				plane.marker.setIcon(symbol);
			}
			// Else position with velocityVector
			else {
				// Update position
				plane.marker.setPosition({
					lat: plane.marker.getPosition().lat() + plane.velocityVector.lat,
					lng: plane.marker.getPosition().lng() + plane.velocityVector.lng
				});
				// position the heading to the current destination
				var path = plane.marker.getPosition();
				var destination = new google.maps.LatLng({
					lat: plane.to.latitude,
					lng: plane.to.longitude
				});
				var heading = google.maps.geometry.spherical.computeHeading(path, destination) - offsetRotation;
				var symbol = plane.marker.getIcon();
				symbol.rotation = heading;
				//change the color of the marker depending on his state

				if (plane.marker.getPosition().lat() == plane.from.latitude && plane.marker.getPosition().lng() == plane.from.longitude) {
					symbol.fillColor = 'green';
				} else if (plane.marker.getPosition().lat() == plane.to.latitude && plane.marker.getPosition().lng() == plane.to.longitude) {
					symbol.fillColor = 'red';
				} else {
					symbol.fillColor = 'black';
				}
				plane.marker.setIcon(symbol);
			}
		}

		/**
		 * Delete a marker on the map
		 * @param {*} marker
		 */
		googleMap.clearMarker = function (marker) {
			if (marker.window) {
				marker.window.close();
				clearInterval(marker.windowRefresh);
			}
			marker.flightPath.setVisible(false);
			marker.setMap(null);
		}

		/**
		 * Delete all planes on the map
		 * @param {*} planes
		 */
		googleMap.clearAllMarkers = function (planes) {
			for (var i in planes) {
				this.clearMarker(planes[i].marker);
			}
		}

		return googleMap;
	})
	.factory('AlgorithmPlane', function () {

		var algorithmPlane = {};

		var degreeTo1Km = (1 / 111.19);

		/**
		 * Add velocity vector to plane
		 * @param {*} plane
		 */
		algorithmPlane.createVelocityVector = function (plane) {

			if (!plane || plane == '') {
				return null;
			}

			// If plane has arrived at its destination, there is no movement
			if (plane.to.longitude == plane.marker.getPosition().lng() &&
				plane.to.latitude == plane.marker.getPosition().lat()) {
				plane.velocityVector = {
					lat: 0,
					lng: 0
				}
				return plane;
			}

			// Direction vector : toPosition - currentPosition
			var directionVector = {
				lng: plane.to.longitude - plane.marker.getPosition().lng(),
				lat: plane.to.latitude - plane.marker.getPosition().lat()
			}

			// Convert Knots to Km/s
			var speedKm = (plane.position.velocity * 1.852) / 3600;

			// Et donc 1° de latitude (arc de méridien) vaut 111,11 km
			// Et donc 1° de longitude (arc de parallèle) vaut 111,11 km * cos latitude
			// Processing of speed : Number of degrees in 1 second
			var speedLat = degreeTo1Km * speedKm;
			var speedLng = (degreeTo1Km * speedKm) * Math.cos(plane.marker.getPosition().lat() * (Math.PI / 180));

			// Normalize velocity
			var length = Math.sqrt((directionVector.lat * directionVector.lat) + (directionVector.lng * directionVector.lng));
			var normalize = {
				lng: directionVector.lng * (1 / length),
				lat: directionVector.lat * (1 / length)
			}

			// Velocity vector : direction * velocity (speed)
			var velocityVector = {
				lat: normalize.lat * speedLat,
				lng: normalize.lng * speedLng
			}

			// If the plane will exceed the destination with this velocity
			if (directionVector.lat > -0.5 && directionVector.lat < 0.5 &&
				directionVector.lng > -0.5 && directionVector.lng < 0.5) {
				// We give the exact velocity value to reach your destination
				velocityVector.lat = directionVector.lat;
				velocityVector.lng = directionVector.lng;
			}

			// Add velocity vector
			plane.velocityVector = velocityVector;
			return plane;
		}

		return algorithmPlane;
	});








//var generateIconCache = {};
function generateStatIcon(color, departure,arrival, callback) {
//console.log("creation imge (param) : "+departure+"   "+arrival+"  "+min+" "+max);

var number = departure+arrival;
console.log("number : "+number);

var imageMaxSize = 60, imageMinSize = 40;
var fontMaxSize = 12, fontMinSize = 14;

/*if (generateIconCache[number] !== undefined) {
	callback(generateIconCache[number]);
}*/

if(color == null || color == ""){
	color = "#2063C6";
}

var fontSize = fontMinSize;// + (number/max) * (fontMaxSize-fontMinSize);
var imageWidth = imageMinSize;// + (number/max) * (imageMaxSize-imageMinSize) ;
var imageHeight = imageWidth;

console.log("creation imge : "+fontSize+"   "+imageHeight+":"+imageWidth+" "+number);

var svg = d3.select(document.createElement('div')).append('svg')
	.attr('viewBox', '0 0 54.4 54.4')
	.append('g')

var circles = svg.append('circle')
	.attr('cx', '27.2')
	.attr('cy', '27.2')
	.attr('r', '21.2')
	.style('fill', color );

var path = svg.append('path')
	.attr('d', 'M27.2,0C12.2,0,0,12.2,0,27.2s12.2,27.2,27.2,27.2s27.2-12.2,27.2-27.2S42.2,0,27.2,0z M6,27.2 C6,15.5,15.5,6,27.2,6s21.2,9.5,21.2,21.2c0,11.7-9.5,21.2-21.2,21.2S6,38.9,6,27.2z')
	.attr('fill', '#FFFFFF');

var text = svg.append('text')
	.attr('dx', 27)
	.attr('dy', 24)
	.attr('text-anchor', 'middle')
	.attr('style', 'font-size:' + fontSize + 'px; fill: #FFFFFF; font-family: Arial, Verdana; font-weight: bold')
	.html(departure);
var text2 = svg.append('text')
	.attr('dx', 27)
	.attr('dy', 40)
	.attr('text-anchor', 'middle')
	.attr('style', 'font-size:' + fontSize + 'px; fill: #FFFFFF; font-family: Arial, Verdana; font-weight: bold')
	.html(arrival);

var svgNode = svg.node().parentNode.cloneNode(true),
	image = new Image();

d3.select(svgNode).select('clippath').remove();

var xmlSource = (new XMLSerializer()).serializeToString(svgNode);

image.onload = (function(imageWidth, imageHeight) {
	var canvas = document.createElement('canvas'),
	context = canvas.getContext('2d'),
	dataURL;

	d3.select(canvas)
	.attr('width', imageWidth)
	.attr('height', imageHeight);

	context.drawImage(image, 0, 0, imageWidth, imageHeight);

	dataURL = canvas.toDataURL();
	//generateIconCache[number] = dataURL;

	callback(dataURL);
}).bind(this, imageWidth, imageHeight);

image.src = 'data:image/svg+xml;base64,' + btoa(encodeURIComponent(xmlSource).replace(/%([0-9A-F]{2})/g, function(match, p1) {
	return String.fromCharCode('0x' + p1);
}));
}
