# Real Time Flight

## Team
* Christophe Bortier
* Roland Bura
* Mamadou Cissé
* Maxime Pirlet
* Antonin Riche 

## Links
* [NodeJS](https://nodejs.org/en/)
* [Meteor](https://www.meteor.com/install)
* [OpenSky-Network](https://opensky-network.org/)